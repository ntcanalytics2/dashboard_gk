#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import gspread

from oauth2client.service_account import ServiceAccountCredentials

scope= ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']

creds= ServiceAccountCredentials.from_json_keyfile_name('PDD Time Analytics-1ec8099afb30.json',scope)

client= gspread.authorize(creds)

yd_df_09_10 = pd.read_excel('Courses 2009-10.xlsx')
yd_df_10_11 = pd.read_excel('Courses 2010-11.xlsx')
yd_df_11_12 = pd.read_excel('Courses 2011-12.xlsx')
yd_df_12_13 = pd.read_excel('Courses 2012-13.xlsx')
yd_df_13_14 = pd.read_excel('Courses 2013-14.xlsx')
yd_df_14_15 = pd.read_excel('Courses 2014-15.xlsx')
yd_df_15_16 = pd.read_excel('Courses 2015-16.xlsx')
yd_df_16_17 = pd.read_excel('Courses 2016-17.xlsx')
yd_df_17_18 = pd.read_excel('Courses 2017-18.xlsx')
yd_df_18_19 = pd.read_excel('Courses 2018-19.xlsx')
yd_df_19_20 = pd.read_excel('Courses 2019-20.xlsx')

frames= [yd_df_09_10,yd_df_10_11,yd_df_11_12,yd_df_12_13,yd_df_13_14,yd_df_14_15,yd_df_15_16,
         yd_df_16_17,yd_df_17_18,yd_df_18_19, yd_df_19_20]
yd_df = pd.concat(frames).reset_index(drop=True)
# yd_df

yd_df.shape

yd_df['# Teacher'] = yd_df['Teacher Code'].apply(lambda x: len(x.split(',')))
# yd_df

yd_df['# Teacher'].sum()

Teacher_Code = yd_df['Teacher Code'].str.split(',', expand=True).values.ravel()
Teacher_Name = yd_df['Teacher Name'].str.split(',', expand=True).values.ravel()
Teacher_Email = yd_df['Teacher Email'].str.split(',', expand=True).values.ravel()
Teacher_Mobile = yd_df['Teacher Mobile No.'].str.split(',', expand=True).values.ravel()
TCD = yd_df['TCD'].str.split(',', expand=True).values.ravel()

SL = np.repeat(yd_df['SL.NO'].values, len(Teacher_Code) / len(yd_df))
Course_ID = np.repeat(yd_df['Course ID'].values, len(Teacher_Code) / len(yd_df))
Course_Type = np.repeat(yd_df['Course Type'].values, len(Teacher_Code) / len(yd_df))
Start_Date = np.repeat(yd_df['Start Date'].values, len(Teacher_Code) / len(yd_df))
End_Date = np.repeat(yd_df['End Date'].values, len(Teacher_Code) / len(yd_df))
# TCD = np.repeat(yd_df['TCD'].values, len(Teacher_Code) / len(yd_df))
State = np.repeat(yd_df['State'].values, len(Teacher_Code) / len(yd_df))
District = np.repeat(yd_df['District'].values, len(Teacher_Code) / len(yd_df))
Institution_name = np.repeat(yd_df['Institution name'].values, len(Teacher_Code) / len(yd_df))
Institution_Email = np.repeat(yd_df['Institution Email'].values, len(Teacher_Code) / len(yd_df))
Institution_Phone = np.repeat(yd_df['Institution Phone'].values, len(Teacher_Code) / len(yd_df))
New_PAX = np.repeat(yd_df['New PAX'].values, len(Teacher_Code) / len(yd_df))
Repeater_PAX = np.repeat(yd_df['Repeater PAX'].values, len(Teacher_Code) / len(yd_df))
Total_PAX = np.repeat(yd_df['Total PAX'].values, len(Teacher_Code) / len(yd_df))

yd_df_split = pd.DataFrame({'SL.No': SL,'Course ID': Course_ID, 'Course Type': Course_Type,
                           'Start Date': Start_Date,'End_Date': End_Date,'Teacher Code': Teacher_Code,
                           'Teacher Name':Teacher_Name,'TCD':TCD,'State':State,'District':District,
                           'Teacher Email':Teacher_Email,'Teacher Mobile':Teacher_Mobile,
                           'Institution name':Institution_name,'Institution Email':Institution_Email,
                           'Institution Phone':Institution_Phone,'New PAX':New_PAX,
                           'Repeater PAX':Repeater_PAX,'Total PAX':Total_PAX})
# yd_df_split

teacher_notnull_df = yd_df_split[yd_df_split['Teacher Code'].notnull()]

# course_count= teacher_notnull_df.groupby(['Course ID']).size().to_frame('Course #').reset_index()
# teacher_notnull_df = pd.merge(teacher_notnull_df,course_count, on='Course ID', how='left')
# teacher_notnull_df

# teacher_notnull_df['PAX Count'] = teacher_notnull_df['Total PAX']/teacher_notnull_df['Course #']
# teacher_notnull_df.columns

teacher_notnull_df['Teacher Name']= teacher_notnull_df['Teacher Name'].str.upper().str.title()
# teacher_notnull_df

# df1 = teacher_notnull_df[teacher_notnull_df['Teacher Name'].str.contains("Jyothi Bohra")]
# df1

# teacher_duplicated = teacher_notnull_df[teacher_notnull_df.duplicated(subset=['Course ID','Course Type', 'Start Date', 'End_Date',
#        'Teacher Code', 'Teacher Name', 'TCD', 'State', 'District','New PAX', 'Repeater PAX',
#        'Total PAX'],keep='first')]
# teacher_duplicated

teacher_notnull_df['Teacher Name'] = teacher_notnull_df['Teacher Name'].str.lstrip()
teacher_notnull_df['Teacher Code'] = teacher_notnull_df['Teacher Code'].str.lstrip()
teacher_notnull_df['TCD'] = teacher_notnull_df['TCD'].str.lstrip()
teacher_notnull_df['Teacher Email'] = teacher_notnull_df['Teacher Email'].str.lstrip()
teacher_notnull_df['Institution name'] = teacher_notnull_df['Institution name'].str.replace('â€‹', '')
# teacher_notnull_df.shape

teacher_notnull_df['Start Date']=pd.to_datetime(teacher_notnull_df['Start Date'])
teacher_notnull_df['End_Date']=pd.to_datetime(teacher_notnull_df['End_Date'])

# teacher_notnull_df.drop_duplicates(subset=['Course ID','Course Type', 'Start Date', 'End_Date',
#        'Teacher Code', 'Teacher Name', 'TCD', 'State', 'District','New PAX', 'Repeater PAX',
#        'Total PAX'], keep='last', inplace=True)

teacher_notnull_df.loc[teacher_notnull_df['District']=='Faridabad','State']='Delhi'
teacher_notnull_df.loc[teacher_notnull_df['District']=='Gautam Buddha Nagar','State']='Delhi'
teacher_notnull_df.loc[teacher_notnull_df['District']=='Gurgaon','State']='Delhi'
teacher_notnull_df.loc[teacher_notnull_df['District']=='Noida','State']='Delhi'
teacher_notnull_df.loc[teacher_notnull_df['District']=='Ghaziabad','State']='Delhi'

teacher_notnull_df.loc[teacher_notnull_df['State']=='Chandigarh','State']='Punjab'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Pondicherry','State']='Tamil Nadu'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Dadra and Nagar Haveli','State']='Gujarat'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Daman and Diu','State']='Gujarat'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Sikkim','State']='West Bengal'

# teacher_notnull_df.to_csv('yd_df_without_SYC.csv',index=False)

# state_dist_syc_df = pd.read_excel('District corresponding with SYC.xlsx')
# state_dist_syc_df.drop_duplicates(subset=['District','State','SYC'], keep='last', inplace=True)

spreadsheet_key_dist_SYC = '1Bj8ZjG3QrCzJDwBOb9UXHPC_tIgiG3zFviYxbNHllew'
sheet_dist_SYC = client.open("District corresponding with SYC").sheet1
table_dist_SYC = sheet_dist_SYC.get_all_values()
state_dist_syc_df= pd.DataFrame(table_dist_SYC[1:], columns=table_dist_SYC[0])
state_dist_syc_df.drop_duplicates(subset=['District','State','SYC'], keep='last', inplace=True)

teacher_notnull_df = teacher_notnull_df.merge(state_dist_syc_df, on=['State','District'])
# teacher_notnull_df

# teacher_notnull_df.loc[teacher_notnull_df['State']=='Chandigarh','State']='Punjab'
# teacher_notnull_df.loc[teacher_notnull_df['State']=='Pondicherry','State']='Tamil Nadu'
# teacher_notnull_df.loc[teacher_notnull_df['State']=='Dadra and Nagar Haveli','State']='Gujarat'
# teacher_notnull_df.loc[teacher_notnull_df['State']=='Daman and Diu','State']='Gujarat'
# teacher_notnull_df.loc[teacher_notnull_df['State']=='Sikkim','State']='West Bengal'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Arunachal Pradesh','State']='North East'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Assam','State']='North East'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Manipur','State']='North East'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Meghalaya','State']='North East'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Nagaland','State']='North East'
teacher_notnull_df.loc[teacher_notnull_df['State']=='Tripura','State']='North East'

teacher_notnull_df.loc[teacher_notnull_df['Course Type']=='3 Days YES!+','Course Type']='YES!+'
teacher_notnull_df.loc[teacher_notnull_df['Course Type']=='Happiness Program For Youth','Course Type']='HPY'

teacher_notnull_df.drop_duplicates(subset=['Course ID','Course Type', 'Start Date', 'End_Date',
       'Teacher Code', 'Teacher Name', 'TCD', 'State', 'District','New PAX', 'Repeater PAX',
       'Total PAX'], keep='last', inplace=True)
# teacher_notnull_df.shape

course_count= teacher_notnull_df.groupby(['Course ID']).size().to_frame('Course #').reset_index()
teacher_notnull_df = pd.merge(teacher_notnull_df,course_count, on='Course ID')
# teacher_notnull_df

teacher_notnull_df['PAX Count'] = teacher_notnull_df['Total PAX']/teacher_notnull_df['Course #']
# teacher_notnull_df

# spreadsheet_key = '1Lkiq1BFOHc33Mil9TGZyvPd96zC06YRKy7t9iwcw-rQ'
# teacher_notnull_df.to_csv('yd_df_1teacher_per_row.csv',index=False)
# content= open('yd_df_1teacher_per_row.csv','r').read()
# client.import_csv('1Lkiq1BFOHc33Mil9TGZyvPd96zC06YRKy7t9iwcw-rQ', content)

spreadsheet_key = '1Lkiq1BFOHc33Mil9TGZyvPd96zC06YRKy7t9iwcw-rQ'
from df2gspread import df2gspread as d2g
wks_name = 'Youth Desk Report'
d2g.upload(teacher_notnull_df, spreadsheet_key, wks_name, credentials=creds, row_names=True)

state_1 = ['Andhra Pradesh','Bihar','Chhattisgarh','Delhi','Goa','Gujarat','Haryana',
        'Himachal Pradesh','Jammu and Kashmir','Jharkhand','Karnataka','Kerala',
        'Madhya Pradesh','Maharashtra','North East','Odisha','Punjab','Rajasthan','Tamil Nadu',
        'Telangana','Uttar Pradesh','Uttarakhand','West Bengal']

spreadsheet_key = '1Lkiq1BFOHc33Mil9TGZyvPd96zC06YRKy7t9iwcw-rQ'
from df2gspread import df2gspread as d2g
for i in state_1:
    teacher_notnull_df_i = teacher_notnull_df[teacher_notnull_df['State']== i]
    wks_name = i
    d2g.upload(teacher_notnull_df_i, spreadsheet_key, wks_name, credentials=creds, row_names=True)

