#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import gspread

from oauth2client.service_account import ServiceAccountCredentials

scope= ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']

creds= ServiceAccountCredentials.from_json_keyfile_name('PDD Time Analytics-1ec8099afb30.json',scope)

client= gspread.authorize(creds)

spreadsheet_key= '117h33Brr8D4OT0WgP26To0ToNa9i7I9t2SRYf9NlNH2c'

sheet= client.open("Delhi Happiness Mahotsav").sheet1

table= sheet.get_all_values()

delhi_hm= pd.DataFrame(table[1:], columns=table[0])

# delhi_hm = pd.read_excel('Delhi Happiness Mahotsav.xlsx')

delhi_hm['Teacher Count'] = delhi_hm['Teachers'].apply(lambda x: len(x.split(',')))
# delhi_hm

Teachers = delhi_hm['Teachers'].str.split(',', expand=True).values.ravel()

ID = np.repeat(delhi_hm['ID'].values, len(Teachers) / len(delhi_hm))
Course_Type = np.repeat(delhi_hm['Course Type'].values, len(Teachers) / len(delhi_hm))
# Address = np.repeat(delhi_hm['Address'].values, len(Teachers) / len(delhi_hm))
City = np.repeat(delhi_hm['City'].values, len(Teachers) / len(delhi_hm))
Postal_Code = np.repeat(delhi_hm['Postal Code'].values, len(Teachers) / len(delhi_hm))
District = np.repeat(delhi_hm['District'].values, len(Teachers) / len(delhi_hm))
IC = np.repeat(delhi_hm['IC'].values, len(Teachers) / len(delhi_hm))
Start_Date = np.repeat(delhi_hm['Start Date'].values, len(Teachers) / len(delhi_hm))
End_Date = np.repeat(delhi_hm['End Date'].values, len(Teachers) / len(delhi_hm))
Phone = np.repeat(delhi_hm['Phone'].values, len(Teachers) / len(delhi_hm))
Last_Updated = np.repeat(delhi_hm['Last Updated'].values, len(Teachers) / len(delhi_hm))

delhi_hm_df = pd.DataFrame({'ID':ID,'Course Type':Course_Type,'Teachers':Teachers,
                            'City':City,'Postal Code':Postal_Code,
                            'District':District,'IC':IC,'Start Date':Start_Date,
                            'End Date':End_Date,'Phone':Phone,'Last Updated':Last_Updated})

# delhi_hm_df

delhi_notnull_df = delhi_hm_df[delhi_hm_df['Teachers'].notnull()]

delhi_notnull_df['Teacher'], delhi_notnull_df['Teacher Code'] = delhi_notnull_df['Teachers'].str.split('(', 1).str
delhi_notnull_df['Teacher Code'] = delhi_notnull_df['Teacher Code'].str.replace(')', '')

delhi_notnull_df['Teacher']= delhi_notnull_df['Teacher'].str.upper().str.title()

# delhi_notnull_df

delhi_notnull_df['Teacher'] = delhi_notnull_df['Teacher'].str.lstrip()
delhi_notnull_df['Teacher Code'] = delhi_notnull_df['Teacher Code'].str.lstrip()

delhi_notnull_df['Start Date']=pd.to_datetime(delhi_notnull_df['Start Date'])
delhi_notnull_df['End Date']=pd.to_datetime(delhi_notnull_df['End Date'])
# delhi_notnull_df

delhi_notnull_df['Last Updated']= delhi_notnull_df['Last Updated'].astype('datetime64[ns]')

# delhi_notnull_df

delhi_notnull_df.drop_duplicates(subset=['ID','Course Type', 'Teachers', 'Address', 
        'City', 'Postal Code','District', 'IC', 'Start Date', 'End Date', 'Phone',
        'Teacher', 'Teacher Code'], keep='last', inplace=True)

# from datetime import datetime
# now = datetime.now()
# delhi_notnull_df['Last Updated']= now.strftime("%m/%d/%Y %H:%M:%S")

# delhi_notnull_df.to_csv('Updated Delhi HM2020.csv')

spreadsheet_key = '1mqZMAIwBs4N37tznn-yvsv_R56LitS1lH6lMzEMyoH4'
from df2gspread import df2gspread as d2g
wks_name = 'Happiness Mahotsav'
d2g.upload(delhi_notnull_df, spreadsheet_key, wks_name, credentials=creds, row_names=True)

delhi_2019_df = pd.read_csv('Delhi 2019 splitted.csv')
delhi_notnull_df['Last Updated'] =  delhi_notnull_df['Last Updated'].astype(str)

delhi_2019_with_PAX = delhi_2019_df[delhi_2019_df['PAX'].notnull()]
delhi_2019_without_PAX = delhi_2019_df[delhi_2019_df['PAX'].isnull()]

# delhi_2019_with_PAX['Teacher Code'].nunique()

delhi_hm_2020 = delhi_notnull_df[['ID', 'Course Type', 'City', 'District','Teacher', 
                'Teacher Code','Start Date', 'End Date','Last Updated']]
# delhi_hm_2020

unique_PAXonly_df = delhi_2019_with_PAX[~delhi_2019_with_PAX['Teacher Code'].isin(delhi_hm_2020['Teacher Code'])]

# unique_PAXonly_df

combined_delhi_PAX_df = pd.concat([unique_PAXonly_df, delhi_notnull_df], ignore_index=True, sort=False)
# combined_delhi_PAX_df['Teacher Code'].nunique()

unique_without_PAX_df = delhi_2019_without_PAX[~delhi_2019_without_PAX['Teacher Code'].isin(combined_delhi_PAX_df['Teacher Code'])]

# combined_delhi_df['Start Date']=pd.to_datetime(combined_delhi_df['Start Date'])
# combined_delhi_df['End Date']=pd.to_datetime(combined_delhi_df['End Date'])
# combined_delhi_df['Last Updated']=pd.to_datetime(combined_delhi_df['Last Updated'])
# combined_delhi_df.shape

# unique_without_PAX_df

combined_delhi_df = pd.concat([unique_without_PAX_df, combined_delhi_PAX_df], ignore_index=True, sort=False)
# combined_delhi_df

combined_delhi_df = combined_delhi_df[['ID', 'Course Type', 'City', 'District', 'Start Date',
       'End Date', 'Teacher', 'Teacher Code', 'PAX', 'Last Updated']]

# combined_delhi_df

last_updated_time = combined_delhi_df['Last Updated'].iloc[-1]
# last_updated_time

combined_delhi_df['Last Updated all'] = last_updated_time

combined_delhi_df['Teacher Code'] = combined_delhi_df['Teacher Code'].str.lstrip()

combined_delhi_df

combined_delhi_df = combined_delhi_df.fillna('')
# combined_delhi_df

combined_delhi_df.to_csv('combined_delhi_df.csv')

spreadsheet_key = '1mqZMAIwBs4N37tznn-yvsv_R56LitS1lH6lMzEMyoH4'
from df2gspread import df2gspread as d2g
wks_name = 'Delhi Combined'
d2g.upload(combined_delhi_df, spreadsheet_key, wks_name, credentials=creds, row_names=True)


# In[ ]:





# In[ ]:




